"""
This file implements an rqt plugin for controlling individual thrusters on Qubo.

Set
"""

import random

from qt_gui.plugin import Plugin
from python_qt_binding import QtCore
from python_qt_binding.QtCore import Qt
from python_qt_binding.QtGui import QIntValidator
from python_qt_binding.QtWidgets import QWidget, QLabel, QSlider, QPushButton, QHBoxLayout, QVBoxLayout, QLineEdit, QDoubleSpinBox

# https://stackoverflow.com/questions/77934193/how-can-i-create-a-simple-gui-using-an-rqt-plugin
class ThrusterControl_Text(Plugin):
    def __init__(self, context):
        super(ThrusterControl_Text, self).__init__(context)

        self._context = context
        self.setObjectName("ThrustersPlugin_Text")

        self._widget = QWidget()

        front_widget = QWidget()
        front = QHBoxLayout(front_widget)
        front_left = ThrusterWidget("Front Left", top_topic="/thruster1", bottom_topic="/thruster2")
        front_right = ThrusterWidget("Front Right", "", "")
        front.addWidget(front_left)
        front.addWidget(front_right)

        back_widget = QWidget()
        back = QHBoxLayout(back_widget)
        back_left = ThrusterWidget("Back Left", "", "")
        back_right = ThrusterWidget("Back Right", "", "")
        back.addWidget(back_left)
        back.addWidget(back_right)

        self._stop_button = QPushButton("Global Stop")
        # When _stop_button's "clicked" signal is emitted, all thruster control widgets
        # will have their stop_top and stop_bottom slots triggered.
        for w in [front_left, front_right, back_left, back_right]:
            self._stop_button.clicked.connect(w.stop_top)
            self._stop_button.clicked.connect(w.stop_bottom)

        layout = QVBoxLayout(self._widget)
        layout.addWidget(self._stop_button)
        layout.addWidget(front_widget)
        layout.addWidget(back_widget)

        context.add_widget(self._widget)

# Widget to control two thrusters (top and bottom one).
class ThrusterWidget(QWidget):
    PWM_STOP = 1500
    PWM_MIN = 1400
    PWM_MAX = 1600

    def __init__(self, title, top_topic, bottom_topic):
        super().__init__()

        self._top_topic = top_topic
        self._bottom_topic = bottom_topic

        title_label = QLabel(title)

        self.top_stop_btn = QPushButton("Stop")
        self.top_pwm_label = QLabel()
        self.top_entry = QDoubleSpinBox()
        self.top_entry.setMaximum(self.PWM_MAX)
        self.top_entry.setMinimum(self.PWM_MIN)
        self.top_entry.setValue(self.PWM_STOP)
        self.top_entry.setDecimals(0)
        self.top_entry.setSingleStep(5)
        self.bottom_entry = QDoubleSpinBox()
        self.bottom_entry.setMaximum(self.PWM_MAX)
        self.bottom_entry.setMinimum(self.PWM_MIN)
        self.bottom_entry.setValue(self.PWM_STOP)
        self.bottom_entry.setDecimals(0)
        self.bottom_entry.setSingleStep(5)
        self.bottom_stop_btn = QPushButton("Stop")
        self.bottom_pwm_label = QLabel()

        top_widget = QWidget()
        top_layout = QHBoxLayout(top_widget)
        top_layout.addWidget(self.top_stop_btn)
        top_layout.addWidget(self.top_pwm_label)

        bottom_widget = QWidget()
        bottom_layout = QHBoxLayout(bottom_widget)
        bottom_layout.addWidget(self.bottom_stop_btn)
        bottom_layout.addWidget(self.bottom_pwm_label)

        self.layout = QVBoxLayout(self)
        self.layout.addWidget(top_widget)
        self.layout.addWidget(self.top_entry)
        self.layout.addWidget(self.bottom_entry)
        self.layout.addWidget(bottom_widget)

        self.top_pwm_label.setText(f"PWM: {self.PWM_STOP}")
        self.top_entry.valueChanged.connect(self.on_top_value_changed)

        self.bottom_pwm_label.setText(f"PWM: {self.PWM_STOP}")
        self.bottom_entry.valueChanged.connect(self.on_bottom_value_changed)

        self.top_stop_btn.clicked.connect(self.stop_top)
        self.bottom_stop_btn.clicked.connect(self.stop_bottom)
    
    @QtCore.Slot(float)
    def on_top_value_changed(self,value):
        self.top_pwm_label.setText(f"PWM: {int(value)}")
        # TODO: Publish on ROS to set actual PWM. Make sure we have a controller to prevent PWM from changing too rapidly.

    @QtCore.Slot(float)
    def on_bottom_value_changed(self,value):
        self.bottom_pwm_label.setText(f"PWM: {int(value)}")
        self.bottom_entry.clear()
        # TODO: Publish on ROS to set actual PWM.

    @QtCore.Slot()
    def stop_top(self):
        self.top_pwm_label.setText(f"PWM: {self.PWM_STOP}")
        self.top_entry.clear()

    @QtCore.Slot()
    def stop_bottom(self):
        self.bottom_pwm_label.setText(f"PWM: {self.PWM_STOP}")
        self.bottom_entry.clear()
