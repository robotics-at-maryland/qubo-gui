import random

from qt_gui.plugin import Plugin
from python_qt_binding import QtCore
# from python_qt_binding.QtWidgets import QWidget, QVBoxLayout, QPushButton, QLabel
import python_qt_binding.QtWidgets as QtWidgets

# https://stackoverflow.com/questions/77934193/how-can-i-create-a-simple-gui-using-an-rqt-plugin
class Plugin2(Plugin):
    def __init__(self, context):
        super(Plugin2, self).__init__(context)

        self._context = context
        self.setObjectName("ThrustersPlugin")

        self._widget = MyWidget()
        self._widget.show()

        context.add_widget(self._widget)

class MyWidget(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        self.hello = ["Hallo Welt", "Hei maailma", "Hola Mundo", "Привет мир"]

        self.button = QtWidgets.QPushButton("Click on this button, me!")
        self.text = QtWidgets.QLabel("Hello World",
                                     alignment=QtCore.Qt.AlignCenter)

        self.layout = QtWidgets.QVBoxLayout(self)
        self.layout.addWidget(self.text)
        self.layout.addWidget(self.button)

        self.button.clicked.connect(self.magic)

    @QtCore.Slot()
    def magic(self):
        self.text.setText(random.choice(self.hello))

# class MyWidget(QWidget):
#     def __init__(self):
#         super(MyWidget, self).__init__()

#         print("MyWidget constructor called...")

#         self.setStyleSheet("background-color: white;")

#         # Create a layout
#         layout = QVBoxLayout(self)

#         # Create a button
#         self.button = QPushButton('Click Me!', self)
#         self.button.clicked.connect(self.on_button_click)

#         # Create a label for text display
#         self.label = QLabel('Hello, World!', self)

#         # Add the button and label to the layout
#         layout.addWidget(self.button)
#         layout.addWidget(self.label)

#          # Set the layout for the widget
#         self.setLayout(layout)

#     def on_button_click(self):
#         self.label.setText('Button Clicked!')