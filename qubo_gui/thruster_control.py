"""
This file implements an rqt plugin for controlling individual thrusters on Qubo.

Set
"""

import random

from qt_gui.plugin import Plugin
from python_qt_binding import QtCore
from python_qt_binding.QtCore import Qt
from python_qt_binding.QtWidgets import QWidget, QLabel, QSlider, QPushButton, QHBoxLayout, QVBoxLayout

# https://stackoverflow.com/questions/77934193/how-can-i-create-a-simple-gui-using-an-rqt-plugin
class ThrusterControl(Plugin):
    def __init__(self, context):
        super(ThrusterControl, self).__init__(context)

        self._context = context
        self.setObjectName("ThrustersPlugin")

        self._widget = QWidget()

        front_widget = QWidget()
        front = QHBoxLayout(front_widget)
        front_left = ThrusterWidget("Front Left", top_topic="/thruster1", bottom_topic="/thruster2")
        front_right = ThrusterWidget("Front Right", "", "")
        front.addWidget(front_left)
        front.addWidget(front_right)

        back_widget = QWidget()
        back = QHBoxLayout(back_widget)
        back_left = ThrusterWidget("Back Left", "", "")
        back_right = ThrusterWidget("Back Right", "", "")
        back.addWidget(back_left)
        back.addWidget(back_right)

        self._stop_button = QPushButton("Global Stop")
        # When _stop_button's "clicked" signal is emitted, all thruster control widgets
        # will have their stop_top and stop_bottom slots triggered.
        for w in [front_left, front_right, back_left, back_right]:
            self._stop_button.clicked.connect(w.stop_top)
            self._stop_button.clicked.connect(w.stop_bottom)

        layout = QVBoxLayout(self._widget)
        layout.addWidget(self._stop_button)
        layout.addWidget(front_widget)
        layout.addWidget(back_widget)

        context.add_widget(self._widget)

# Widget to control two thrusters (top and bottom one).
class ThrusterWidget(QWidget):
    PWM_STOP = 1500

    def __init__(self, title, top_topic, bottom_topic):
        super().__init__()

        self._top_topic = top_topic
        self._bottom_topic = bottom_topic

        title_label = QLabel(title)

        self.top_stop_btn = QPushButton("Stop")
        self.top_pwm_label = QLabel()
        self.top_slider = QSlider(Qt.Horizontal)
        self.bottom_slider = QSlider(Qt.Horizontal)
        self.bottom_stop_btn = QPushButton("Stop")
        self.bottom_pwm_label = QLabel()

        top_widget = QWidget()
        top_layout = QHBoxLayout(top_widget)
        top_layout.addWidget(self.top_stop_btn)
        top_layout.addWidget(self.top_pwm_label)

        bottom_widget = QWidget()
        bottom_layout = QHBoxLayout(bottom_widget)
        bottom_layout.addWidget(self.bottom_stop_btn)
        bottom_layout.addWidget(self.bottom_pwm_label)

        self.layout = QVBoxLayout(self)
        self.layout.addWidget(top_widget)
        self.layout.addWidget(self.top_slider)
        self.layout.addWidget(self.bottom_slider)
        self.layout.addWidget(bottom_widget)

        self.top_slider.setTickPosition(QSlider.TicksBothSides)
        self.top_slider.setTickInterval(20)
        self.top_slider.setMinimum(1400)
        self.top_slider.setMaximum(1600)
        self.top_slider.setValue(1500)
        self.top_pwm_label.setText(f"PWM: {1500}")
        self.top_slider.valueChanged.connect(self.on_top_value_changed)

        self.bottom_slider.setTickPosition(QSlider.TicksBothSides)
        self.bottom_slider.setTickInterval(20)
        self.bottom_slider.setMinimum(1400)
        self.bottom_slider.setMaximum(1600)
        self.bottom_slider.setValue(1500)
        self.bottom_pwm_label.setText(f"PWM: {1500}")
        self.bottom_slider.valueChanged.connect(self.on_bottom_value_changed)

        self.top_stop_btn.clicked.connect(self.stop_top)
        self.bottom_stop_btn.clicked.connect(self.stop_bottom)
    
    @QtCore.Slot(int)
    def on_top_value_changed(self, value):
        self.top_pwm_label.setText(f"PWM: {value}")
        # TODO: Publish on ROS to set actual PWM. Make sure we have a controller to prevent PWM from changing too rapidly.

    @QtCore.Slot(int)
    def on_bottom_value_changed(self, value):
        self.bottom_pwm_label.setText(f"PWM: {value}")
        # TODO: Publish on ROS to set actual PWM.

    @QtCore.Slot()
    def stop_top(self):
        self.top_slider.setValue(self.PWM_STOP)

    @QtCore.Slot()
    def stop_bottom(self):
        self.bottom_slider.setValue(self.PWM_STOP)