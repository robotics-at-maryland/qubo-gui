# Setup

(1) Dependency installation:

```
sudo apt install python3-pip
python3 -m pip install inputs
```

(2) Clone into your ROS workspace, for example:

```
cd ~/ros_ws/src
git clone git@code.umd.edu:robotics-at-maryland/qubo_gui.git
```

(3) Build your ROS workspace:

```
cd ~/ros_ws
colcon build
```

(4) Tell rqt to find our plugins (if rqt doesn't seem to be finding updated plugin code, try running this again):

```
rqt --force-discover
```

(5) For now, run `rqt` from the commandline. Then load the plugins you want inside the rqt GUI.

- Click "Plugins" in the menu bar.
- Find the plugin you want.

## Building and running the code

```
colcon build
```

# Development

## Adding or modifying a plugin

- Update `plugin.xml`
    - You need one `<class>` tag per plugin.
    - Make sure `type=` attribute has the correct Python import path to refer to your plugin class. 

Suggested naming convention: Organize under a "Qubo" group, so that it is easy to find our custom plugins in rqt. For example:

```xml
...

<class ...>
    ...
    <qtgui>
      <group>
        <label>Qubo Control</label>
      </group>
      <label>Thruster Control</label>
      ...
    </qtgui>
  </class>
```